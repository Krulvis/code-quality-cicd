import argparse
import subprocess

if __name__ == "__main__":
    args = argparse.ArgumentParser()
    args.add_argument("-T", "--title", required=True, help="Title of the commit")
    args.add_argument("-S", "--sha", required=True, help="SHA of the commit")
    args = vars(args.parse_args())
    sha = args['sha']
    print(f"Getting metrics for commit: {args['title']} with SHA: {sha}")
    output = subprocess.run("git show -U0 " + str(sha), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    diff = output.stdout.decode('UTF-8', errors="ignore")
    print(diff)
